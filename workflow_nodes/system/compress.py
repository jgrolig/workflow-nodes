# Copyright 2021 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import sys
import os
import shutil

from xmlhelpy import Choice
from xmlhelpy import option
from xmlhelpy.types import String

from .main import system


@system.command(
    version="0.1.0",
    description="Compress node to compress folders or files",
)
@option(
    "compresstarget",
    char="c",
    description="Folder to be compressed."
    "If folder path contains whitespaces"
    "please put the path in quotes.",
    required=True,
    param_type=String
)
@option(
    "outputpath",
    char="p",
    description="Name and path of target folder",
)
@option(
    "compressiontype",
    char="t",
    param_type=Choice(["gztar", "zip"]),
    description="Type of compression",
    default="zip"
)
@option(
    "force_overwrite",
    char="o",
    is_flag=True
)
def compress(compresstarget, outputpath, compressiontype, force_overwrite):
    """Wrapper node compressing files of folders"""

    if outputpath:
        if f".{compressiontype:s}" in outputpath:
            outputpath = outputpath[:-(len(compressiontype)+1)]
        tpath = outputpath
    else:
        tpath = os.path.join(os.getcwd(), "compressedfile")

    compresstarget = rf"{compresstarget:s}"

    compressitem = os.path.basename(os.path.normpath(compresstarget))
    compresspath = os.path.dirname(compresstarget)

    if force_overwrite:
        shutil.make_archive(tpath, compressiontype, compresspath, compressitem)
        print(f"Created {compressiontype:s} file "
        f"\"{os.path.basename(os.path.normpath(tpath)):s}"
        f".{compressiontype:s}\" from the folder/file \"{compressitem:s}\""
        f" on path \"{compresspath:s}\"", file=sys.stderr)
    else:
        if os.path.isfile(tpath+"."+compressiontype):
            print("File already exists and won't be overwritten!"
            " If you want to overwrite it then please activate"
            " the flag \"force_overwrite\".", file=sys.stderr)
        else:
            shutil.make_archive(tpath, compressiontype, compresspath, compressitem)
            print(f"Created {compressiontype:s} file "
        f"\"{os.path.basename(os.path.normpath(tpath)):s}"
        f".{compressiontype:s}\" from the folder/file \"{compressitem:s}\""
        f" on path \"{compresspath:s}\"", file=sys.stderr)
