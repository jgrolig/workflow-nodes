# Copyright 2021 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import subprocess
import sys

from click.types import DateTime
from xmlhelpy import argument
from xmlhelpy import Choice
from xmlhelpy import option

from .main import system


@system.command(
    version="0.1.0",
    description="Matlab node for using Matlab scripts",
)
@argument(
    "file",
    description="File to be executed",
    required=True,
)
@option(
    "startup-folder",
    char="f",
    description="Change the matlab startup folder to defined path",
)
# @option(
#     "execution-mode",
#     char="m",
#     var_name="mode",
#     description="Choose the desired execution mode:"
#     " single=single execution of script in bash"
#     " and Matlab closes automatically after runthrough;"
#     " bash=bash execution of script and matlab stays open after runthrough;"
#     " desktop=execution of script in Matlab desktop"
#     " and matlab stays open after runthrough",
#     default="single",
#     param_type=Choice(["single", "bash", "desktop"]),
# )
@option(
    "functionarguments",
    char="a",
    description="Add arguments if file is a function",
)
@option(
    "saveoutputstofile",
    char="s",
    description="Save outputs to given file",
)
@option(
    "variablestosave",
    char="v",
    description="Variables to save to file." " Variables should be separated by commas",
)
@option(
    "loadfile",
    char="l",
    description="Load given file",
)
@option(
    "printoutput",
    char="p",
    description="Print variable to stdout",
)
def matlab(
    file,
    startup_folder,
    functionarguments,
    saveoutputstofile,
    loadfile,
    variablestosave,
    printoutput,
):
    """Wrapper node for Matlab."""

    cmd = ["matlab"]
    if startup_folder:
        cmd += ["-sd", startup_folder]

    # if loadfile or saveoutputstofile:
    #     cmd += ["-nodesktop -nosplash -r"]
    # else:
    #     cmd += ["-batch"]

    cmd += ["-nodesktop -nosplash -r"]

    if loadfile:
        if "," in loadfile:
            if "[" in loadfile:
                loadfile.replace("[", "")
                loadfile.replace("]", "")
            elif "(" in loadfile:
                loadfile.replace("(", "")
                loadfile.replace(")", "")
            filestoload = (loadfile.replace(" ", "")).split(",")
            if ".mat" not in filestoload[0]:
                filestoload[0] = "{}.mat".format(filestoload[0])
            cmd += ["'load " + filestoload[0] + ";"]
            for i in filestoload[1:]:
                if ".mat" not in i:
                    i = f"{i}.mat"
                cmd += ["load " + i + ";"]
        else:
            if ".mat" not in loadfile:
                loadfile = f"{loadfile}.mat"
            cmd += ["'load " + loadfile + ";"]
    else:
        cmd += ["'"]

    if functionarguments:
        if "(" not in functionarguments:
            functionarguments = f"({functionarguments})"
        cmd += [file + functionarguments + ";"]
    else:
        cmd += [file + ";"]

    if saveoutputstofile:
        if ".mat" not in saveoutputstofile:
            saveoutputstofile = f"{saveoutputstofile}.mat"
        if variablestosave:
            if "[" in variablestosave:
                variablestosave.replace("[", "")
                variablestosave.replace("]", "")
            elif "(" in variablestosave:
                variablestosave.replace("(", "")
                variablestosave.replace(")", "")

            variables = (variablestosave.replace(" ", "")).split(",")
            cmd += ['save("' + saveoutputstofile + '",']
            for i in variables[:-1]:
                cmd += ['"' + i + '",']
            cmd += ['"' + variables[-1] + '");']
        else:
            cmd += ['save("' + saveoutputstofile + '");']

    if printoutput:
        cmd += ['fprintf("%d",' + printoutput + "); exit'"]
        # print(subprocess.getoutput(cmd), file=sys.stdout)
    else:
        cmd += ["exit'"]

    # if mode == "single":
    #     cmd += ["-batch"]
    # elif mode == "bash":
    #     cmd += ["-nodesktop -nosplash -r"]
    # elif mode == "desktop":
    #     cmd += ["-r"]

    print(" ".join(cmd), file=sys.stderr)
    sys.exit(subprocess.call(cmd))
